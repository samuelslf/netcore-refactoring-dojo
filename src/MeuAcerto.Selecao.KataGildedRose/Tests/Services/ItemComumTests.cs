﻿using MeuAcerto.Selecao.KataGildedRose.Services;
using MeuAcerto.Selecao.KataGildedRose.Services.Spec;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests.Services
{
    public class ItemComumTests
    {
        readonly IItem _itemService;
        public ItemComumTests()
        {
            _itemService = new ItemComum();
        }

        [Fact]
        public void Envelhecer_ItemPassado_DeveDiminuirQualidadeDuasVezes()
        {
            // Arrange
            const int qualidadeInicial = 10;
            const int qualidadeEsperada = 8;

            var item = new Item()
            {
                PrazoParaVenda = -1,
                Qualidade = qualidadeInicial
            };

            // Act
            _itemService.Envelhecer(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeEsperada);
        }

        [Fact]
        public void Envelhecer_ItemNaoPassado_DeveDiminuirSomenteUmaQualidade()
        {
            // Arrange
            const int qualidadeInicial = 10;
            const int qualidadeEsperada = 9;

            var item = new Item()
            {
                PrazoParaVenda = 1,
                Qualidade = qualidadeInicial
            };

            // Act
            _itemService.Envelhecer(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeEsperada);
        }
    }
}
