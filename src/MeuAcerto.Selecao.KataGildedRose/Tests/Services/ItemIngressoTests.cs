﻿using MeuAcerto.Selecao.KataGildedRose.Services;
using MeuAcerto.Selecao.KataGildedRose.Services.Spec;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests.Services
{
    public class ItemIngressoTests
    {
        readonly IItem _itemService;
        public ItemIngressoTests()
        {
            _itemService = new ItemIngresso();
        }

        [Fact]
        public void Envelhecer_ItemIngressoComQualidadeMaiorQue10_DeveAumentarQualidadeEmUm()
        {
            // Arrange
            const int qualidadeInicial = 10;
            const int qualidadeEsperada = 11;

            var item = new Item()
            {
                PrazoParaVenda = 11,
                Qualidade = qualidadeInicial
            };

            // Act
            _itemService.Envelhecer(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeEsperada);
        }

        [Theory]
        [InlineData(10)]
        [InlineData(9)]
        [InlineData(8)]
        [InlineData(7)]
        [InlineData(6)]
        public void Envelhecer_ItemIngressoComQualidadeMenorIgualQue10MaiorQue5_DeveAumentarQualidadeEmDois(int prazoVenda)
        {
            // Arrange
            const int qualidadeInicial = 10;
            const int qualidadeEsperada = 12;

            var item = new Item()
            {
                PrazoParaVenda = prazoVenda,
                Qualidade = qualidadeInicial
            };

            // Act
            _itemService.Envelhecer(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeEsperada);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(4)]
        [InlineData(3)]
        [InlineData(2)]
        [InlineData(1)]
        public void Envelhecer_ItemIngressoComQualidadeMenorIgualA5_DeveAumentarQualidadeEmTres(int prazoVenda)
        {
            // Arrange
            const int qualidadeInicial = 10;
            const int qualidadeEsperada = 13;

            var item = new Item()
            {
                PrazoParaVenda = prazoVenda,
                Qualidade = qualidadeInicial
            };

            // Act
            _itemService.Envelhecer(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeEsperada);
        }

        [Fact]
        public void Envelhecer_ItemIngressoComQualidadeMenorQueZero_DeveZearQualidade()
        {
            // Arrange
            const int qualidadeEsperada = 0;

            var item = new Item()
            {
                PrazoParaVenda = -1,
                Qualidade = 50
            };

            // Act
            _itemService.Envelhecer(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeEsperada);
        }
    }
}
