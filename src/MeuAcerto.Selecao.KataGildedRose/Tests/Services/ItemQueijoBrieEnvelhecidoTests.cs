﻿using MeuAcerto.Selecao.KataGildedRose.Services;
using MeuAcerto.Selecao.KataGildedRose.Services.Spec;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests.Services
{
    public class ItemQueijoBrieEnvelhecidoTests
    {
        readonly IItem _itemService;
        public ItemQueijoBrieEnvelhecidoTests()
        {
            _itemService = new ItemQueijoBrieEnvelhecido();
        }

        [Fact]
        public void Envelhecer_ItemPassado_DeveAumentarQualidadeDuasVezes()
        {
            // Arrange
            const int qualidadeInicial = 10;
            const int qualidadeEsperada = 12;

            var item = new Item()
            {
                PrazoParaVenda = -1,
                Qualidade = qualidadeInicial
            };

            // Act
            _itemService.Envelhecer(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeEsperada);
        }

        [Fact]
        public void Envelhecer_ItemNaoPassado_DeveAumentarSomenteUmaQualidade()
        {
            // Arrange
            const int qualidadeInicial = 10;
            const int qualidadeEsperada = 11;

            var item = new Item()
            {
                PrazoParaVenda = 1,
                Qualidade = qualidadeInicial
            };

            // Act
            _itemService.Envelhecer(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeEsperada);
        }
    }
}
