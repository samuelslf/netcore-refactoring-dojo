﻿using MeuAcerto.Selecao.KataGildedRose.Services;
using MeuAcerto.Selecao.KataGildedRose.Services.Spec;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests.Services
{
    public class ItemConjuradoTests
    {
        readonly IItem _itemService;
        public ItemConjuradoTests()
        {
            _itemService = new ItemConjurado();
        }

        [Fact]
        public void Envelhecer_ItemConjurado_DeveDiminuirQualidadeEmDois()
        {
            // Arrange
            const int qualidadeInicial = 10;
            const int qualidadeEsperada = 8;

            var item = new Item()
            {
                PrazoParaVenda = 5,
                Qualidade = qualidadeInicial
            };

            // Act
            _itemService.Envelhecer(item);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeEsperada);
        }
    }
}
