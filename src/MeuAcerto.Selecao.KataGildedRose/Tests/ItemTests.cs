﻿using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class ItemTests
    {
        [Fact]
        public void Item_Envelhecer_DeveDiminuirOPeriodoEmUm()
        {
            // Arrange
            const int prazoParaVendaInicial = 10;

            var item = new Item()
            {
                PrazoParaVenda = prazoParaVendaInicial
            };

            // Act
            item.EnvelhecerUmDia();

            // Assert
            Assert.Equal(item.PrazoParaVenda, prazoParaVendaInicial - 1);
        }

        [Fact]
        public void Item_ZerarQualidade_DeveDiminuirAQualidadeAZero()
        {
            // Arrange
            const int qualidadeInicial = 10;

            var item = new Item()
            {
                Qualidade = qualidadeInicial
            };

            // Act
            item.ZerarQualidade();

            // Assert
            Assert.True(item.Qualidade == 0);
        }

        [Theory]
        [InlineData(10, 1)]
        [InlineData(5, 3)]
        [InlineData(1, 0)]
        public void Item_DiminuirQualidade_DeveDiminuirDeAcordoComOEsperado(int qualidadeInicial, int decrescimo)
        {
            // Arrange
            var item = new Item()
            {
                Qualidade = qualidadeInicial
            };

            // Act
            item.DiminuirQualidade(decrescimo);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeInicial - decrescimo);
        }

        [Theory]
        [InlineData(10, 1)]
        [InlineData(5, 3)]
        [InlineData(1, 0)]
        public void Item_AumentarQualidade_DeveAumentarDeAcordoComOEsperado(int qualidadeInicial, int acrescimo)
        {
            // Arrange
            var item = new Item()
            {
                Qualidade = qualidadeInicial
            };

            // Act
            item.AumentarQualidade(acrescimo);

            // Assert
            Assert.Equal(item.Qualidade, qualidadeInicial + acrescimo);
        }

        [Theory]
        [InlineData(-1, true)]
        [InlineData(0, false)]
        [InlineData(1, false)]
        public void Item_VerificarProdutoPassado_DeveRetornarDeAcordoComOEsperado(int prazo, bool statusEsperado)
        {
            // Arrange
            var item = new Item()
            {
                PrazoParaVenda = prazo
            };

            // Act
            // Assert
            Assert.Equal(item.Passado(), statusEsperado);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-2)]
        public void Item_VerificarPodeDiminuirQualidade_DeveNaoPermitir(int qualidade)
        {
            // Arrange
            var item = new Item()
            {
                Qualidade = qualidade
            };

            // Act
            // Assert
            Assert.False(item.PodeDiminuirQualidade(item.Qualidade * -1));
        }

        [Fact]
        public void Item_VerificarPodeDiminuirQualidade_Permitir()
        {
            // Arrange
            var item = new Item()
            {
                Qualidade = 1
            };

            // Act
            // Assert
            Assert.True(item.PodeDiminuirQualidade(item.Qualidade));
        }
    }
}
