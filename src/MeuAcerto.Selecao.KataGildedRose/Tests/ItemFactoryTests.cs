﻿using MeuAcerto.Selecao.KataGildedRose.Services;
using MeuAcerto.Selecao.KataGildedRose.Services.Spec;
using System;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class ItemFactoryTests
    {
        readonly IItemFactory _itemFactory;
        public ItemFactoryTests()
        {
            _itemFactory = new ItemFactory();
        }

        [Theory]
        [InlineData("Elixir do Mangusto")]
        [InlineData("Corselete +5 DEX")]
        public void ObterInstancia_NomeProdutoDesconhecido_DeveInstanciaItemComum(string nomeProduto)
        {
            // Arrange
            // Act
            var instancia = _itemFactory.ObterInstancia(nomeProduto);

            // Assert
            Assert.Equal(typeof(ItemComum), instancia.GetType());
        }

        [Fact]
        public void ObterInstancia_NomeProdutoLendario_DeveInstanciaLendario()
        {
            // Arrange
            string nomeProduto = "Dente do Tarrasque";

            // Act
            var instancia = _itemFactory.ObterInstancia(nomeProduto);

            // Assert
            Assert.Equal(typeof(ItemLendario), instancia.GetType());
        }

        [Fact]
        public void ObterInstancia_NomeProdutoConjurado_DeveInstanciaConjurado()
        {
            // Arrange
            string nomeProduto = "Bolo de Mana Conjurado";

            // Act
            var instancia = _itemFactory.ObterInstancia(nomeProduto);

            // Assert
            Assert.Equal(typeof(ItemConjurado), instancia.GetType());
        }

        [Fact]
        public void ObterInstancia_NomeProdutoQueijo_DeveIntanciarQueijoBrieEnvelhecido()
        {
            // Arrange
            string nomeProduto = "Queijo Brie Envelhecido";

            // Act
            var instancia = _itemFactory.ObterInstancia(nomeProduto);

            // Assert
            Assert.Equal(typeof(ItemQueijoBrieEnvelhecido), instancia.GetType());
        }

        [Fact]
        public void ObterInstancia_NomeProdutoIngresso_DeveIntanciarIngresso()
        {
            // Arrange
            string nomeProduto = "Ingressos para o concerto do Turisas";

            // Act
            var instancia = _itemFactory.ObterInstancia(nomeProduto);

            // Assert
            Assert.Equal(typeof(ItemIngresso), instancia.GetType());
        }

        [Fact]
        public void ObterInstancia_NomeProdutoVazio_DeveLancarExcecao()
        {
            // Arrange
            // Act
            // Assert
            Assert.Throws<ArgumentNullException>(() => _itemFactory.ObterInstancia(""));
        }
    }
}
