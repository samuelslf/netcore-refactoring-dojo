﻿namespace MeuAcerto.Selecao.KataGildedRose
{
    public static class ItemExtension
    {
        public static void DiminuirQualidade(this Item item, int decrescimo = 1)
        {
            if (item.PodeDiminuirQualidade(decrescimo))
            {
                item.Qualidade = item.Qualidade -= decrescimo;
            }
        }

        public static void AumentarQualidade(this Item item, int acrescimo = 1)
        {
            if (item.Qualidade + acrescimo <= 50)
                item.Qualidade += acrescimo;
        }

        public static void ZerarQualidade(this Item item)
        {
            item.Qualidade = 0;
        }

        public static void EnvelhecerUmDia(this Item item)
        {
            item.PrazoParaVenda--;
        }

        public static bool Passado(this Item item) => item.PrazoParaVenda < 0;
        public static bool PodeDiminuirQualidade(this Item item, int decrescimo)
            => (item.Qualidade - decrescimo) >= 0;
    }
}
