﻿using MeuAcerto.Selecao.KataGildedRose.Services;
using MeuAcerto.Selecao.KataGildedRose.Services.Spec;
using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        readonly IItemFactory _itemFactory;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
            _itemFactory = new ItemFactory();
        }

        public void AtualizarQualidade()
        {
            for (var i = 0; i < Itens.Count; i++)
            {
                var item = Itens[i];
                var itemImplementacao = _itemFactory.ObterInstancia(item.Nome);
                itemImplementacao.Envelhecer(item);
            }
        }
    }
}
