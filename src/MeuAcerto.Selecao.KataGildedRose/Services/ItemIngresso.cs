﻿using MeuAcerto.Selecao.KataGildedRose.Services.Spec;

namespace MeuAcerto.Selecao.KataGildedRose.Services
{
    public class ItemIngresso : IItem
    {
        public void Envelhecer(Item item)
        {
            item.AumentarQualidade();

            if (item.PrazoParaVenda <= 10)
            {
                item.AumentarQualidade();
            }

            if (item.PrazoParaVenda <= 5)
            {
                item.AumentarQualidade();
            }

            item.EnvelhecerUmDia();

            if (item.PrazoParaVenda < 0)
            {
                item.ZerarQualidade();
            }
        }
    }
}
