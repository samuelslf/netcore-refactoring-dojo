﻿using MeuAcerto.Selecao.KataGildedRose.Services.Spec;

namespace MeuAcerto.Selecao.KataGildedRose.Services
{
    class ItemQueijoBrieEnvelhecido : IItem
    {
        public void Envelhecer(Item item)
        {
            item.AumentarQualidade();
            item.EnvelhecerUmDia();

            if (item.Passado())
                item.AumentarQualidade();
        }
    }
}
