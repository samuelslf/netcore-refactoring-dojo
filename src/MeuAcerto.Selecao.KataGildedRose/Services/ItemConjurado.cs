﻿using MeuAcerto.Selecao.KataGildedRose.Services.Spec;

namespace MeuAcerto.Selecao.KataGildedRose.Services
{
    public class ItemConjurado : IItem
    {
        public void Envelhecer(Item item)
        {
            item.DiminuirQualidade(2);
            item.EnvelhecerUmDia();
        }
    }
}
