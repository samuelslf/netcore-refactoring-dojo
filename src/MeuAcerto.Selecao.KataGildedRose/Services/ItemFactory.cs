﻿using System;
using MeuAcerto.Selecao.KataGildedRose.Services.Spec;

namespace MeuAcerto.Selecao.KataGildedRose.Services
{
    public class ItemFactory : IItemFactory
    {
        public IItem ObterInstancia(string nomeItem)
        {
            if (string.IsNullOrEmpty(nomeItem))
                throw new ArgumentNullException(nomeItem);

            switch (nomeItem.ToUpper())
            {
                case "BOLO DE MANA CONJURADO":
                    return new ItemConjurado();
                case "INGRESSOS PARA O CONCERTO DO TURISAS":
                    return new ItemIngresso();
                case "DENTE DO TARRASQUE":
                    return new ItemLendario();
                case "QUEIJO BRIE ENVELHECIDO":
                    return new ItemQueijoBrieEnvelhecido();
                default:
                    return new ItemComum();
            }
        }
    }
}
