﻿using MeuAcerto.Selecao.KataGildedRose.Services.Spec;

namespace MeuAcerto.Selecao.KataGildedRose.Services
{
    public class ItemComum : IItem
    {
        public void Envelhecer(Item item)
        {
            item.DiminuirQualidade();
            item.EnvelhecerUmDia();

            if (item.Passado())
                item.DiminuirQualidade();
        }
    }
}