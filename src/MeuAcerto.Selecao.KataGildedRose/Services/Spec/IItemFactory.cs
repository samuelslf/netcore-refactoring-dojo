﻿namespace MeuAcerto.Selecao.KataGildedRose.Services.Spec
{
    public interface IItemFactory
    {
        IItem ObterInstancia(string nomeItem);
    }
}
